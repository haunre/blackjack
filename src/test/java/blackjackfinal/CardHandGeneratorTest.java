/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package blackjackfinal;

import finalblackjack.Card;
import java.util.Random;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author levansen
 */
public class CardHandGeneratorTest {
    
    public CardHandGeneratorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of generateHand method, of class CardHandGenerator.
     */
    @Test
    public void testGenerateHand() {
        int numCards = 4;
       Card[] cardHand = new Card[numCards];
        Random random = new Random();
        
        for(int i=0;i<cardHand.length;i++){
            //getting a random value from 1-12
            int value = random.nextInt(12)+1;
            String suit = Card.SUITS[random.nextInt(3)];
            
            //Create an instance of card and assign it to the array
            Card card = new Card(value, suit);
            cardHand[i] = card;
         }
         Card[] expected = cardHand;
          Card[] result = cardHand;
        assertArrayEquals(expected, result);
        // TODO review the generated test code and remove the default call to fail.

    }
    
}


